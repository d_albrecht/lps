/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package core

class BfsSolverTest extends TestBase {
	type Fundament = (Int)
	type State = (Int, Int)
	
	"The breadth-first search solver" should {
		"provide the initial state after instantiation" in {
			val game = new Game[Fundament, State] {
				val stateMap: (State) => String = _.toString
				def listPossibleActions(level: Level[Fundament, State], state: State): List[String] = ???
				def executeAction(level: Level[Fundament, State], state: State, action: String): (OriginalStatus, State) = ???
			}
			val level = Level[Fundament, State]((0), (1, 2))
			
			val uut = new BfsSolver(game, level)
			
			uut.getAllStates() should be (List(RegularStep(0, "", active, (1, 2))))
		}
		
		"provide (no) progress after instantiation" in {
			val game = new Game[Fundament, State] {
				val stateMap: (State) => String = _.toString
				def listPossibleActions(level: Level[Fundament, State], state: State): List[String] = ???
				def executeAction(level: Level[Fundament, State], state: State, action: String): (OriginalStatus, State) = ???
			}
			val level = Level[Fundament, State]((0), (1, 2))
			
			val uut = new BfsSolver(game, level)
			
			uut.getProgress should be ((false, 1))
		}
	}
}