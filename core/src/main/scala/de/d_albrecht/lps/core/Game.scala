/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package core

/** packs all that defines a game */
trait Game[Fundament, State]{
	/** defines how a state should be compared to previously generated states */
	val stateMap: (State) => String
	/** should return all possible actions that can be performed on the given state.
		actions are represented by strings. an action must be reconstructable from a string
		that contains concatenated actions
		(aka no two sequence of different actions may result in the same sequence of characters) */
	def listPossibleActions(level: Level[Fundament, State], state: State): List[String]
	/** should return the new state and status after the given action is performed
		on the given state */
	def executeAction(level: Level[Fundament, State], state: State, action: String): (OriginalStatus, State)
}

/** describes a level of some puzzle game.
	the fundamentals describe everything that can never change about a level. They are always true.
	The state describes everything that can and probably will change
	throughout the process of solving a level. each level comes with its initial state */
case class Level[Fundament, State](val fundamentals: Fundament, val initState: State)
