/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package core

class BfsSolver[Fundament, State](game: Game[Fundament, State], level: Level[Fundament, State], limitStorage: Boolean = false) extends Solver[State] {
	private val registry = if (limitStorage) new SetStepRegistry(game.stateMap) else new MapStepRegistry(game.stateMap)
	private var fringe: List[Step[State]] = List(registry.register(RegularStep(0, "", active, level.initState)))
	private var stepLog: List[Step[State]] = fringe
	private def log(steps: List[Step[State]]): Unit = {
		stepLog ++= (if (limitStorage) {
			steps.filter(_.status match { case _: TerminalStatus => true; case _ => false })
		} else {
			steps
		})
	}
	def getAllStates(): List[Step[State]] = stepLog
	def getProgress: (Boolean, Int) = (stepLog.exists(_.status == solved), fringe.length)
	/** takes a single step of the fringe and expands on it. if the fringe is empty, nothing happens. the response will indicate whether the solver has found a solution yet and how many elements the fringe still contains */
	def doSingleStep(): Unit = {
		fringe match {
			case Nil => /* nothing to step */
			case head :: tail => {
				val newSteps = game.listPossibleActions(level, head.state).map(a => (a, game.executeAction(level, head.state, a))).map(newState => head.doStep(newState._1, newState._2._1, newState._2._2)).map(registry.register(_))
				log(newSteps)
				fringe = tail ++ newSteps.filter(s => s.status == active)
			}
		}
	}
	def doMultipleSteps(limit: Option[Int] = None): Unit = {
		//TODO will not take the limit into account at the moment
		expandFringe()
	}
	/** expands all the elements of the fringe one time. if the fringe is empty, nothing happens. the response will indicate whether the solver has found a solution yet and how many elements the fringe still contains */
	def expandFringe(): Unit = {
		val newSteps = fringe.flatMap(f => game.listPossibleActions(level, f.state).map(a => (a, game.executeAction(level, f.state, a))).map(newState => f.doStep(newState._1, newState._2._1, newState._2._2))).map(registry.register(_))
		log(newSteps)
		fringe = newSteps.filter(s => s.status == active)
		//println((fringe.size, stepLog.size, fringe.headOption.map(s => s.round).getOrElse(-1)))
	}
	/** attempts to solve the given level. it processes the fringe in waves. if the search shouldn't be done exhaustively, the solver will stop as soon as one expansion yielded at least one solution. otherwise the solver will stop when no new steps are available */
	def solve(exhaustive: Boolean): Unit = {
		def shouldDoStep: Boolean = {
			val progress = getProgress
			progress._2 > 0 && (!progress._1 || exhaustive)
		}
		while (shouldDoStep) {
			doMultipleSteps()
		}
	}
}