/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package core

/** describes the state of a level.
	it contains all mutual elements of this level and their current values
	in this step of the solving process */
trait Step[State] {
	val round: Int
	val path: String
	val status: Status
	val state: State
	def doStep(action: String, status: OriginalStatus, state: State): Step[State] = {
		RegularStep(round + 1, path + action, status, state)
	}
	def asDuplicateOf(orig: Step[State]) = DuplicateStep[State](this, orig)
	def fullName(): String = s"${this.round}-${this.path}.${this.status.rep}"
}

case class RegularStep[State](round: Int, path: String, status: OriginalStatus, state: State) extends Step[State]

/** contains a step that was encountered before and its original (initial) copy */
case class DuplicateStep[State](dup: Step[State], orig: Step[State]) extends Step[State] {
	val round = dup.round
	val path = dup.path
	val status = reoccurrent
	val state = dup.state
}

/** describes the status of a state and whether it has to be further solved or not */
sealed trait Status { val rep: String }
/** describes any status that is not cuplicate*/
sealed trait OriginalStatus extends Status
/** describes terminal statuses that don't need/allow further solving one way or another */
sealed trait TerminalStatus(val rep: String) extends OriginalStatus
/** use this status to indicate a solved puzzle.
	the solver might not be able to recognize otherwise */
case object solved extends TerminalStatus("slvd")
/** use this status to indicate a puzzle that isn't solvable anymore
	because of reasons the solver can't determine itself */
case object failed extends TerminalStatus("fail")
/** use this status to indicate a puzzle that doesn't allow further actions.
	specify to the solver (IN A FUTURE VERSION) if your game simulator will annotate this status or not.
	if not the solver will flag them (THE SOLVER CAN'T AT THE MOMENT */
case object stuck extends TerminalStatus("stck")
/** describes non terminal statuses that need further solving */
sealed trait NonTerminalStatus(val rep: String) extends OriginalStatus
/** describes the default status of a puzzle that should be solved further */
case object active extends NonTerminalStatus("actv")
/** describes duplicate stasatuses that were encountered before.
	solving them would result in no benefits as the identical "original"
	will be / was solved further */
sealed trait DuplicateStatus(val rep: String) extends Status
/** describes a state that the solver encountered before.
	the game simulator is not supposed to determine this itself, the solver will.
	from all identical states only the first encountered will be further solved,
	the remaining are flagged reoccuring */
case object reoccurrent extends DuplicateStatus("rocr")
