/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package core

trait StepRegistry[State] {
	def register(s: Step[State]): Step[State]
}

/** the accurate StepRgistry,
		it will flag every dublicate step with the exact original step,
		with this information you will be able to re-create the whole tree/graph of steps analyzed,
		but it will consume more memory to do so
*/
protected class MapStepRegistry[State](stateMap: (State) => String) extends StepRegistry[State] {
	private var registry: Map[String, Step[State]] = Map()
	def register(s: Step[State]): Step[State] = {
		registry.get(stateMap(s.state)).map(s.asDuplicateOf(_)).getOrElse{
			registry = registry + (stateMap(s.state) -> s); s
		}
	}
}

/** the bare minimum implementation of a StepRgistry,
		it will flag every dublicate step with itself as the original step,
		but it will consume less memory to do so
*/
protected class SetStepRegistry[State](stateMap: (State) => String) extends StepRegistry[State] {
	private var registry: Set[String] = Set()
	def register(s: Step[State]): Step[State] = {
		if (registry(stateMap(s.state)))
			s.asDuplicateOf(s)
		else {
			registry += stateMap(s.state)
			s
		}
	}
}

trait Solver[State] {
	/** returns all discovered states, if the solver was asked to store everything.
			in case the solver has limited its memory footprint,
			the list should at least contain all terminal states */
	def getAllStates(): List[Step[State]]
	/** returns the progress of the solving process.
			the first value defines whether some solved state was discovered.
			the second value describes the expandable states */
	def getProgress: (Boolean, Int)
	/** expand a single state */
	def doSingleStep(): Unit
	/** tells the solver to do some amount of steps.
			which states exactly get expanded is up to the solver.
			the limit is a maximum, the solver may do less for whatever reason */
	def doMultipleSteps(limit: Option[Int] = None): Unit
	/** tells the solver to process states until the puzzle is solved.
			if the solving should be exhaustive,
			the solver will only stop when all states are stepped through */
	def solve(exhaustive: Boolean): Unit
}