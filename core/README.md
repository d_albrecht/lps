Logic Puzzle Solver - core
==========================

This folder contains the core elements of the Logic Puzzle Solver. These are mostly interfaces that need to be implemented by the concrete puzzle game or common fixtures that the solver uses.

The usual use case for this part of the solver is to apply it to new kinds of puzzles by adapting the solver interfaces to the logic of the new puzzle. But you can also work on the solver and implement different strategies how the solver should prioritize certain states.

The core project comes with a BFS solver. Another heuristical solver is soon available via the `heuristical`-project that makes use of a static value function that judges individual states.

## Usage

To apply the solver to new games, the interface of interest is the Game-trait. The solver usually will take care of most other things already, but the solver won't know how your particular game works.

### Game state

The first step when applying the solver to a new game is to determine what a game state looks like. The solver distinguishes two different kinds of state information. Those that are literally fundamental to a level, information that will under no circumstances be able to change, and the part of the game state that is altered to represent a new state. If you can't find a way to encode anything as fundamental, then you can always put everything into the changing state-type. But this will always come with the drawvback, that you might have dublicate information that is stored with every state and cause the solver to run out of memory more quickly.

Usually what can be considered fundamental state information is stuff like the map/grid layout on which the game takes place. Or the victory condition if this condition is specific to every level.

Part of the changing state information is stuff like your avatar's position, in games that feature a movable avatar. Or objects that have to be moved to vertain victory cells. Or opponents in puzzles that include some.

### State serialization

If you have defined your game state types, the next step is a serialization of this game state into a string representation. This representation can have any arbitrary form, the essential part here is that the same state (no matter how you reached it), should have an identical representation. The solver will rely on this representation to discard dublicate states.

### Actions

The third step is to define what kinds of actions are possible between states and how they are represented. The game will be asked throughout the solving process what actions are possible in each state. These actions will be encoded by strings. And depending on the game these strings can have any arbitrary form. The only thing to note with the action encoding is that each state will be labeled with the sequence of actions that led to this state. To be able to decode the seqence afterwards (if you actually want to try them out in the real game), you have to be able to differenciate the individual actions.

One example for puzzles with a grid and a moving avatar is to use directions of movement (north, east, west, south, up, down, right, left). In games with (semi-)fixed actuators, you can encode their possition in a spreadsheet manner (A1, B5). If everything else fails, you can always terminate each action with a delimiter. So, an action could always end in a comma sign, or a period, or whatnot. This way, the sequence can easily be split at these delimiters to retrieve the actions taken.

### Performing an action

The final step is to simply encode the derivation of one state from another and the used action. If your game features automated opponents, these would move alongside. If your game had other triggers that would cause various effects, these would be triggered here, too. As the solver doesn't know your game, one step of your game state has to encode everything there is to change.

The game implementation should be stateless to enable parallel processing. This isn't necessary today as the current solver doesn't make use of parallel execution, but this still is part of the "interface".

## Solving levels

To apply the solver to your game logic take a look at the example tests given in the other applied games. The basic idea is to instantiate the solver, provide an instance of your game, prepare the initial game state that the solver should start with and then let it run from there. Also, see the other use cases for further discussions of things to keep an eye on, like memory usage, as this probably is the biggest problem you could run into.