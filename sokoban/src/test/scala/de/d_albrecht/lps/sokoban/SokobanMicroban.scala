/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package sokoban

import core._

class SokobanMicroban extends TestBase {
	"From the Microban levels of Sokoban the solver" should {
		"be able to solve level 001" in {
			val level = SokobanTest.level(
				"####..",
				"#.O#..",
				"#..###",
				"#@+..#",
				"#..a.#",
				"#..###",
				"####.."
			)
			
			SokobanTest(level) should be (true)
		}
	}
}