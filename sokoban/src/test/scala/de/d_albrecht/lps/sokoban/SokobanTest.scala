/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package sokoban

import core._
import Sokoban._

object SokobanTest {
	def apply(level: Level[Fundament, State], exhaustive: Boolean = false): Boolean = {
		val game = new SokobanGame()
		val solver = new BfsSolver(game, level)
		solver.solve(exhaustive)
		val solutions = solver.getAllStates().filter(_ match {
			case RegularStep(_, _, `solved`, _) => true
			case DuplicateStep(_, RegularStep(_, _, `solved`, _)) => true
			case _ => false
		})
		//solutions.map(_.fullName()).foreach(println)
		solutions.size > 0
	}

	/**
	 * # denotes a blocked cell aka a wall
	 * O (capital O) denotes an empty target cell
	 * @ denotes a target cell with object
	 * a denotes an object
	 * X (capital X) denotes the player on a target cell
	 * + denotes the player usually
	 * all else denote empty cells
	 */
	def level(lines: String*): Level[Fundament, State] = {
		val h = lines.size
		if (h == 0) {
			Level(Fundament((0, 0), Set(), Set()), State((0, 0), Set()))
		} else {
			val w = lines.map(_.size).max(Ordering.Int)
			
			val cells: Seq[(Char, Int, Int)] = lines.zipWithIndex.flatMap{case (l, y) => l.toCharArray().toList.zipWithIndex.map{case (c, x) => (c, x, y)}}
			val types = cells.groupBy(_._1).withDefault(_ => Seq[(Char, Int, Int)]())

			val blocked = types('#').map(c => (c._2, c._3)).toSet
			val targets = (types('O') ++ types('@') ++ types('X')).map(c => (c._2, c._3)).toSet
			val player = (types('+') ++ types('X')).map(c => (c._2, c._3)).head
			val objects = (types('a') ++ types('@')).map(c => (c._2, c._3)).toSet
			
			Level(Fundament((w, h), blocked, targets), State(player, objects))
		}
	}
}

class SokobanFixtureTest extends TestBase {
	"The SokobanTestFixture" should {
		"generate valid and intuitive levels" in {
			val representation = List(
				".###.",
				"#Oa.#",
				"#@..#",
				".#+#.",
				"#.#.#"
			)

			val expected = Level(Fundament((5, 5), Set((0, 1), (0, 2), (0, 4), (1, 0), (1, 3), (2, 0), (2, 4), (3, 0), (3, 3), (4, 1), (4, 2), (4, 4)), Set((1, 1), (1, 2))),
				State((2, 3), Set((1, 2), (2, 1))))

			SokobanTest.level(representation: _*) should be (expected)
		}
		"generate valid levels with player on target cell" in {
			val representation = List(
				".#.",
				"#X#",
				".#."
			)

			val expected = Level(Fundament((3, 3), Set((0, 1), (1, 0), (1, 2), (2, 1)), Set((1, 1))), State((1, 1), Set()))

			SokobanTest.level(representation: _*) should be (expected)
		}
		"generate valid empty levels" in {
			val representation = List(
				"+"
			)

			val expected = Level(Fundament((1, 1), Set(), Set()), State((0, 0), Set()))

			SokobanTest.level(representation: _*) should be (expected)
		}
	}
}