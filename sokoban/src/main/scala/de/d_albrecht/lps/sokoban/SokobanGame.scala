/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package sokoban

import core._
import Sokoban._

/** Represents a game of Sokoban with all the methods that the solver expects. */
class SokobanGame extends Game[Fundament, State] {
    /** pulls the state serialiation from the SokobanLevel */
    val stateMap: (State) => String = SokobanLevel.stateMap
    private val directions: List[(String, Pos)] = List(("n", (0, -1)), ("e", (1, 0)), ("w", (-1, 0)), ("s", (0, 1)))
    private def isLegalMove(level: Fundament, state: State, dir: Pos): Boolean = {
        val dest = (state.player._1 + dir._1, state.player._2 + dir._2)
        val pshd = (state.player._1 + 2 * dir._1, state.player._2 + 2 * dir._2)
        // destination is walkable and
        !level.blockedCells.contains(dest) && (
            // it is either empty or
            !state.objects.contains(dest) || 
            // it contains an object that is movable
            !level.blockedCells.contains(pshd) && !state.objects.contains(pshd)
        )
    }
    /** processes a state and returns all possible actions */
    def listPossibleActions(level: Level[Fundament, State], state: State): List[String] = {
        directions.filter(d => isLegalMove(level.fundamentals, state, d._2)).map(_._1)
    }
    /** checks if every target cell is occupied by an object */
    private def isSolved(level: Fundament, state: State): Boolean = {
        level.targetCells.forall(trgt => state.objects.contains(trgt))
    }
    /** Attempts to move in a certain direction and possibly push an object around */
    def executeAction(level: Level[Fundament, State], state: State, action: String): (OriginalStatus, State) = {
        val dir = directions.filter(_._1 == action).head._2
        val newPos = (state.player._1 + dir._1, state.player._2 + dir._2)
        val pshPos = (state.player._1 + 2 * dir._1, state.player._2 + 2 * dir._2)
        val newState = State(newPos, state.objects.map(o => if newPos == o then pshPos else o))
        /*
            in the future this method might want to check if a state is meant to be flagged as "failed"
            if the movable objects (not cornered ones) are at least as many as the empty target cells
            but that's an obtimization step, because levels that fulfill this condition are playable nonetheless.
            Sokoban levels technically can never be "stuck"
        */
        (if isSolved(level.fundamentals, newState) then solved else active, newState)
    }
}