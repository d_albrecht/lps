/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package sokoban

import core._
import Sokoban._

/** Describes a level of Sokoban as well as means to conveniently instantiate them. */
object SokobanLevel {
    def apply(fundament: Fundament, player: Pos, objects: Pos*): Level[Fundament, State] = Level(fundament, State(player, objects.toSet))
    val stateMap: (State) => String = (state) => s"${state.player}_${state.objects.toList.sorted.map(_.toString()).mkString("(", ",", ")")}"
}