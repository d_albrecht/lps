Logic Puzzle Solver for Sokoban
===============================

This project contains the application of the solver framework to the mobile game [Sokoban](https://github.com/mobilepearls/com.mobilepearls.sokoban).

The implementation works as well for other variants of the same principle (not just the linked game), but the test cases for this project are from this specific implementations.

I'm not associated with the game or its developers in any way.

## Flowit's objective

To quickly outline the objective of Sokoban - in case you lived under a rock, seriously, who doesn't know this game - is to move objects onto target locations on a cartesian grid. The player avatar can freely move in all four directions, assuming there are no walls on the adjacent cell he wants to step on. If the avatar is next to an object, he can move it in his moving direction, assuming that the next cell behind the object is neither a wall nor occupied by another object. The avatar can only push the objects, never pull them or move them sideways (from the avatar's point of view), and never be on the same cell as an object. All objects and all target locations are interchangeable, there is no fixed requirement of which object has to be placed on which target location. If an object is on a target location, it can still be moved and it isn't locked in place.

For more details, please check the game itself.

## Adapting the Flowit game to the solver Framework

As described by the framework description you have to model and implement several aspects of the actual puzzle game to be able to solve a level. In this segment I will describe the process and decisions as detailed as possible.

### Game state

For Sokoban the game states can clearly be split into static parts and dynamic parts because the level map itself never changes. So the avatar and the objects are part of the dynamic aspect, everything else is the static portion.

#### Fundament

A Sokoban game level is defined by the map size and the state of each cell in these bounderies. A cell can have three states: normal walkable cells, blocked cells (walls), and target cells which are also walkable. The map size includes an outer ring that closes off the grid with walls.

#### Variable state

The variable state part essentially is the position of the player and the objects.

### State serialization

For the serialization of the game state, the only additional difficulty is that the objects' states (if there are more then one) need a deterministic order. Besides this, the serialization is a simple concatenation of state tuples.

### Actions

The possible actions in each state are the movements in any of the cardinal directions (North, East, West, South). But the game will filter out any action that is inherently illegal aka stepping onto walls or walkign towards objects if they can't be pushed in this direction.

### Performing an action

Performing a step is - given the previous modeling - pretty straight forward and probably exactly the same that the original game does. Walk in the given direction, check if an object was pushed in the process and in case move the object onto the next cell in the moving direction.

## Test cases

I've included some (mostly random) selection of game levels as test cases.