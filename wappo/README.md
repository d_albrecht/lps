Logic Puzzle Solver for Wappo
=============================

This project contains the application of the solver framework to the mobile game [Wappo](https://play.google.com/store/apps/details?id=com.pinkzero.wappogame).

I'm not associated with the game or its developers in any way.

## Wappo's objective

To quickly outline the objective of Wappo - in case you don't know the game and don't want to check the game out right now - is to move your avatar to your home. You play on a cartesian grid that has walls between some cells. More advanced levels also feature traps, fires, and teleporters. Teleporters let your avatar instantly move to a different location. Fires and traps will both kill you instantly. To make the game actually interesting, there are opponents, monsters, that hunt you down and will do a couple of steps for every step you take. They can't use the teleporters and will also be killed by the fires, but if they enter the traps, they will be trapped for some turns. The opponents have a strict moving rule. If possible they will first move to the same column as you. If they can't move horizontally or if they already are in the same colum, they will try to move vertically to match your row. If two or more opponents are in the same cell, they will merge and afterwards be able to move more steps. If the opponents catch you, you have lost the level and have to retry.

For more details, please check the game itself.

## Adapting the Wappo game to the solver Framework

As described by the framework description you have to model and implement several aspects of the actual puzzle game to be able to solve a level. In this segment I will describe the process and decisions as detailed as possible.

### Game state

For Wappo the game states can clearly be split into static parts and dynamic parts because the level map itself is non-destructable. So any avatars are part of the dynamic aspect, everything else is the static portion.

#### Fundament

A Wappo game level is defined by the map size, the location of the target cell, placements of walls, traps, fires, and teleporters. Some of these parts are only in the challenge mode levels but the state will always be able to represent them. The level map is surrounded by an implicit wall except if the target cell is outside the actual dimensions, then the player can leave the grid dimensions if and only if they would land on the target cell. But in any case, the map's perimeter is never stored as walls.

#### Variable state

The variable state part essentially is the position of the player and the opponents. With the adjustment that the opponents need more than just their grid position. Because they need more information to describe them. On the one hand, opponents can be able to walk different numbers of cells after each player movement. On the other hand, opponents can get trapped and will then be locked in place for a few turns.

### State serialization

For the serialization of the game state, the only additional difficulty is that the opponents' states (if there are more then one) need a deterministic order. Besides this, the serialization is a simple concatenation of state tuples.

### Actions

The possible actions in each state are the movements in any of the cardinal directions (North, East, West, South). But the game will filter out any action that is inherently illegal aka leaving the map or stepping through walls. Though, the game won't filter out actions, that get yourself killed one way or another. Handling getting killed is part of the action's execution.

### Performing an action

Performing a step is - given the previous modeling - pretty straight forward and probably exactly the same that the original game does. Walk in the given direction, check for victory condition, check for any death conditions, check for teleportations of the player (with some of these things done several times in various orders), move the opponents. Moving the opponents is a rather complex aspect of Wappo, but I won't go into it more at this point as - again - it strictly follows the same rules that the game uses itself.

## Test cases

I've included some (mostly random) selection of game levels as test cases.