/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package wappo

import core._
import Wappo._

class WappoGameTest extends TestBase {
	"The Wappo game" should {
		"be able to create string representations for states" in {
			val uut = new WappoGame()
			
			val state = State((1, 2), Set((3, 4, 2, 2), (4, 5, 3, 0)))
			
			uut.stateMap(state) should be ("(1,2)_((3,4,2,2),(4,5,3,0))")
		}
	}
}