/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package wappo

import core._
import Wappo._

class WappoClassic extends TestBase {
	"From the classic levels of Wappo the solver" should {
		"be able to solve level 000" in {
			val fundament = Fundament((6, 6), (7, 3), List((1, 1)), List((1, 2), (1, 3), (1, 4)))
			val player = (3, 5)
			val hunters = List((1, 2))
			
			WappoTest(WappoLevel(fundament, player, hunters : _*)) should be (true)
		}
		
		"be able to solve level 001" in {
			val fundament = Fundament((6, 6), (4, 7), List((2, 2), (4, 1), (6, 1)), List((5, 2), (5, 6)), List((2, 2)))
			val player = (4, 2)
			val hunters = List((6, 2))
			
			WappoTest(WappoLevel(fundament, player, hunters : _*)) should be (true)
		}
	}
}