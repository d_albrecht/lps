/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package wappo

import core._
import Wappo._

class WappoHuntersTest extends TestBase {
	"The Wappo Hunters" should {
		"prefer horizontal movement" in {
			val fundament = Fundament((5, 5), (6, 3))
			
			val player = (3, 3)
			val hunters = Set((1, 1, 2, 0), (5, 5, 2, 0))
			val expected = Set((3, 1, 2, 0), (3, 5, 2, 0))
			
			WappoHunters.moveHunters(fundament, player, hunters) should be (expected)
		}
	}
}