/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package wappo

import core._
import Wappo._

/** Represents a game of Wappo with all the methods that the solver expects. */
class WappoGame extends Game[Fundament, State] {
    /** pulls the state serialiation from the WappoLevel */
	val stateMap: (State) => String = WappoLevel.stateMap
	private val directions: List[(String, Pos)] = List(("n", (0, -1)), ("e", (1, 0)), ("w", (-1, 0)), ("s", (0, 1)))
	private def isLegalMove(level: Fundament, start: Pos, move: Pos): Boolean = {
		if (move._1 == 0 || move._2 == 0 || move._1 > level.size._1 || move._2 > level.size._2) {
			move == level.home
		} else {
			if (start._1 == move._1) {
				!level.hWalls.contains((start._1, Math.min(start._2, move._2)))
			} else {
				!level.vWalls.contains((Math.min(start._1, move._1), start._2))
			}
		}
	}
	def listPossibleActions(level: Level[Fundament, State], state: State): List[String] = {
		directions.filter(d => isLegalMove(level.fundamentals, state.player, (state.player._1 + d._2._1, state.player._2 + d._2._2))).map(_._1)
	}
	def executeAction(lvl: Level[Fundament, State], state: State, action: String): (OriginalStatus, State) = {
		val level = lvl.fundamentals
		val dir = directions.filter(_._1 == action).head._2
		val newPos = (state.player._1 + dir._1, state.player._2 + dir._2)
		val tpPos: Pos = if (level.teleporter.exists(tp => List(tp._1, tp._2).contains(newPos))) {level.teleporter.map(tp => List(tp._1, tp._2).filter(_ != newPos).head).get} else {newPos}
		val threats = state.hunters.map(h => (h._1, h._2)).union(level.traps.toSet).union(level.fires.toSet)
		if (threats.contains(newPos)) {
			(failed, State(newPos, state.hunters))
		} else if (newPos == level.home) {
			(solved, State(newPos, state.hunters))
		} else if (threats.contains(tpPos)) {
			(failed, State(tpPos, state.hunters))
		} else if (tpPos == level.home) {
			(solved, State(tpPos, state.hunters))
		} else {
			val newHunters = WappoHunters.moveHunters(level, tpPos, state.hunters)
			val newState = State(tpPos, newHunters)
			if (newHunters.exists(h => (h._1, h._2) == tpPos)) {
				(failed, newState)
			} else if (listPossibleActions(lvl, newState).size == 0) {
				(stuck, newState)
			} else {
				(active, newState)
			}
		}
	}
}