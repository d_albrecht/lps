/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package wappo

import Wappo._

object WappoHunters {
	type MovingHunter = (Int, Int, Int, Int, Int)
	private def hunter2Moving(h: Hunter): MovingHunter = {
		(h._1, h._2, h._3, h._4, if (h._4 == 0) h._3 else 0)
	}
	private def moving2Hunter(h: MovingHunter): Hunter = {
		(h._1, h._2, h._3, h._4)
	}
	private def trapHunter(level: Fundament, hunter: MovingHunter): MovingHunter = {
		if (hunter._3 == 2 && level.traps.contains((hunter._1, hunter._2))) {
			(hunter._1, hunter._2, hunter._3, 4, 0) 
		} else {
			hunter
		}
	}
	private def incinerateHunter(level: Fundament, hunter: MovingHunter): Boolean = {
		level.fires.contains((hunter._1, hunter._2))
	}
	private def mergeHunters(hunters: List[MovingHunter]): List[MovingHunter] = {
		hunters.groupBy(h => (h._1, h._2)).toList.flatMap{case (c, h) => if (h.size > 1) {List((c._1, c._2, 3, 0, 0))} else {h}}
	}
	private def stepHunter(level: Fundament, player: Pos, h: MovingHunter): MovingHunter = {
		if (h._5 == 0) {
			h
		} else {
			if (h._1 < player._1 && !level.vWalls.contains((h._1, h._2))) {
				trapHunter(level, (h._1 + 1, h._2, h._3, h._4, h._5 - 1))
			} else if (player._1 < h._1 && !level.vWalls.contains((h._1 - 1, h._2))) {
				trapHunter(level, (h._1 - 1, h._2, h._3, h._4, h._5 - 1))
			} else if (h._2 < player._2 && !level.hWalls.contains((h._1, h._2))) {
				trapHunter(level, (h._1, h._2 + 1, h._3, h._4, h._5 - 1))
			} else if (player._2 < h._2 && !level.hWalls.contains((h._1, h._2 - 1))) {
				trapHunter(level, (h._1, h._2 - 1, h._3, h._4, h._5 - 1))
			} else {
				(h._1, h._2, h._3, h._4, 0)
			}
		}
	}
	@scala.annotation.tailrec
	private def stepHunters(level: Fundament, player: Pos, hunters: List[MovingHunter]): List[MovingHunter] = {
		val advHunters = mergeHunters(hunters.map(stepHunter(level, player, _)).filterNot(incinerateHunter(level, _)))
		if (advHunters.exists(_._5 > 0) && !advHunters.exists(h => (h._1, h._2) == player)) {
			stepHunters(level, player, advHunters)
		} else {
			advHunters
		}
	}
	private def decTrapCount(hunter: Set[Hunter]): Set[Hunter] = {
		hunter.map(h => if (h._4 == 0) {h} else {(h._1, h._2, h._3, h._4 - 1)})
	}
	def moveHunters(level: Fundament, player: Pos, hunters: Set[Hunter]): Set[Hunter] = {
		if (hunters.exists(_._4 == 0)) {
			decTrapCount(stepHunters(level, player, hunters.toList.map(hunter2Moving)).map(moving2Hunter).toSet)
		} else {
			decTrapCount(hunters)
		}
	}
}