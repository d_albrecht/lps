/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps

object Wappo {
	type Pos = (Int, Int) // x, y
	type Hunter = (Int, Int, Int, Int) // x, y, speed, trapped
	case class Fundament(size: Pos, home: Pos,
		hWalls: List[Pos] = List(), vWalls: List[Pos] = List(), traps: List[Pos] = List(),
		teleporter: Option[(Pos, Pos)] = None, fires: List[Pos] = List())
	case class State(player: Pos, hunters: Set[Hunter])
}