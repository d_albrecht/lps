/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package heuristical

/**
		An implementation of a PriorityQueue that works by explicitly assigning a numerical priority to every element.
		According to this priority the queue can provide the element with the highest priority or all elements of the highest priority.
		This implementation will be stable as in the first element enqueued with some priority will be the first element to be dequeeued when no higher priority element is left.
		The implementation requires that the numerical priority can't suddenly change.
*/
trait PriorityQueue[T] {
	def enqueue(e: T): Unit
	def enqueue(es: T*): Unit
	def dequeueSingle(): T
	def dequeueRank(): Seq[T]
}


class RankedPriorityQueue[T](extractor: (T) => Int) extends PriorityQueue[T] {
	private var ranks: Set[Int] = Set()
	private var queue: Map[Int, List[T]] = Map().withDefault(_ => Nil)
	def enqueue(e: T): Unit = {
		val rank = extractor(e)
		ranks += rank
		queue += rank -> (queue(rank) :+ e)
	}
	def enqueue(es: T*): Unit = es.foreach(enqueue)
	def dequeueSingle(): T = {
		val high = ranks.max(Ordering.Int)
		queue(high) match {
			case Nil => {
				// this should never happen, but for good measure...
				ranks -= high
				dequeueSingle()
			}
			case el :: Nil => {
				ranks -= high
				queue -= high
				el
			}
			case el :: rest => {
				queue += high -> rest
				el
			}
		}
	}
	def dequeueRank(): Seq[T] = {
		val high = ranks.max(Ordering.Int)
		ranks -= high
		val rank = queue(high)
		queue -= high
		rank
	}
}
