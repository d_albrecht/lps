Global / onChangedBuildSource := ReloadOnSourceChanges

ThisBuild / scalaVersion := "3.2.1"
ThisBuild / organization := "de.d_albrecht"

lazy val commonSettings = Seq(
	libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.12",
	libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.12" % Test,
	libraryDependencies += "org.scalatestplus" %% "junit-4-13" % "3.2.12.0" % "test",
	libraryDependencies += "junit" % "junit" % "4.13.2" % Test,
)

lazy val core = (project in file("core"))
	.settings(commonSettings)

lazy val heuristical = (project in file("heuristical"))
	.settings(commonSettings)
	.dependsOn(core)
	.dependsOn(core % "test->test")

lazy val wappo = (project in file("wappo"))
	.settings(commonSettings)
	.dependsOn(core)
	.dependsOn(core % "test->test")

lazy val flowit = (project in file("flowit"))
	.settings(commonSettings)
	.dependsOn(core)
	.dependsOn(core % "test->test")

lazy val sokoban = (project in file("sokoban"))
	.settings(commonSettings)
	.dependsOn(core)
	.dependsOn(core % "test->test")