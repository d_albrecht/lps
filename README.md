Logic Puzzle Solver
===================

This repository contains a generic solver framework for certain logic puzzles. Each sub-folder is either the actual framework or example use cases show-casing how the framework can be applied to specific puzzle games.

## Motivation

I'm very into logic puzzles. And I mostly want to prove to myself that I can solve them without external resources like guides or partial solutions from others. But still, at times you get stuck with certain levels. Mostly because the attempt to solve levels of logic puzzles LOVE to lead to a myriad of reachable states. In one such instance I wanted a tool to prove that the initial sequence of actions I was very confident was the only possible start for this level actually WAS the only option. And besides, that there actually is a solution in the first place. That was the point where I created an initial version specific for the Wappo Android game. That worked like a charm but couldn't be called a framework yet.

Later on I discovered other puzzle games and then again was stuck in some levels. So, the idea evolved to generalize the puzzle solver from this one game to a variety of puzzle games. That was when this project came to live.

## Structure

In this repo you will find several folders each with a (somewhat) self-contained sbt-project. First, there is the `core`-folder containing the actual framework. That is mostly interfaces and the solver logic. Besides this folder are other folders that contain applications of the framework for specific games. They will rely on the framework folder. At some point I want to create a visualization of the solution that the solver found as a sequence of states/actions to take to reach the solution (and what detours may exist), which will become its own folder, but this doesn't exist yet.

I have worked with Scala for a long time now, but only very recently made the step to Scala 3. And I never really got comfortable with sbt. So, I decided to use this project to get more familiar with both. Therefore, it is very possible that there are better ways to organize the sbt-projects for example. But as long as the projects build and run, I'm fine for now.

## Basic Principle

The framework is very flexible! And that is by design. There are only very few technical requirements that a puzle game has to fulfill to be compatible with the framework. The problem is that time and memory usually is limited. The actual design is better described in the framework folder. For now, let my outline the basic principle:

Every puzzle game has states of some sort. That essentially is all you can see at one time. And then there are actions you can take in each state that will then result in another state. If you think that through, you can span a tree of states that are all reachable from the initial state of a level. One (or several) of them will fulfill some game- or level-specific criteria that signalize that you have solved this level. Based on this fundamental modeling, the framework's principle to solve logic puzzles is to effectively perform a graph search on this tree.

## Unsuitable puzzle games

As I hinted at before, you can apply the framework to any game with a somewhat limited search-space of possible states. And when you know how you model your game state, you can always estimate the maximum number of possible states very easily. But on the one hand, I wanted to give some examples of games (or game genres) that aren't suitable and on the other hand, point out games where this rather brute-force approach is just really stupid to fall back to. Because brute-force should always be the last resort. And the solver's default search pattern is a breadth-first search because for most logic puzzles it is quite hard to come up with a heuristic that describes the closeness to the solution. And without such a heuristic perforing things like A* is impossible.

I haven't exactly calculated the memory footprint, but stepping through several million states with a few gigabytes of memory at first glance seems pretty reasonable. And that happens pretty quickly (unless you exceed your memory and the garbage collector runs 80% of the time). But at the same time this is the order of magnitute beyond that the solver will struggle to continue. So, if one final state is reachable before exceeding several million states analyzed (depending on your available memory and on how you model your game state), the game (or at least this level) is solvable.

### Enormous search space

Therefore, games like Sudoku that - for the most part - can be solved with pure deduction, should not be solved with this solver. (Classical) 9x9-Sudokus have a absolute maximum of game states of 9^81 = 1.97e77 and you will most likely only discover a solved state after reaching every single state. And also: there are very reasonable deduction rules for Sudoku so even thinking about solving Sudokus by pure brute-force could be considered a sin. (And I know that even the most sophisticated strategies for Sudoku sometimes leave no other choice than to guess, but this should always be a small part of an otherwise deduction based algorithm.)

### Non-discreet states

Another aspect that lets the game states explode is non-discreet values. If part of your game state is an integer (within some range) you have some amount of freedom in this dimension. When the same information is described by a fraction type, the freedom is way larger. The amount of freedom never is actually infinite (you are either limited by the accuracy of the numerical storage of the puzzle game or things like pixels), but the reachable game states easily increase tenfold. Let's imagine Pacman. There, your opponents - the ghosts - don't move with the same speed as you. And you can stand still and let the ghosts move on their own. Pacman has other problems that I will come to later on, but just this one aspect is a reason to not attempt to solve Pacman with this framework. Because you can of course model your own position as an integer, but the non-disceed positions of the ghosts will result in lots of slightly different states that will each be analyzed further. With each state in Pacman resulting in up to five new states (up, down, left, right, stand still).

Games like most Mario games are even wilder in this aspect because there even the player avatar itself has a very complex system to represent the position. I'm no expert in this field, but Mario (at least in some games) has a position, a sub-pixel counter, and a speed variable that all affect the exact behavior of the player's avatar. Resulting in a not processable amount of states.

### (Pseudo-)Multiplayer

The solver framework is flexible enough that you can solve either single player games (like Sudoku), or games against an ai (like Pacman), or even real multi-player games (like Chess). The problem with the latter two types of games is that the framework won't distinguish players but just look at states and invocable actions. Depending on whether you perform an exhaustive search or not, you might discover desirable states that fulfill your winning criteria, but that aren't possible or at least likely in an actual game. Because the ai or your human opponent will (almost) never actually take an action that the solver deemed technically possible. That's because whether the adaption of your puzzle game to this framework tries to solve a level for one player or for either, it will never use a different (opposing) strategy with the other party. As such, the solver will always at first find solutions where both/all parties work together to let one player win. That's not really realistic neither with human multi-player games nor with games against an ai. In all of these cases, the stragety to solve these puzzle games should always be based of some kind of min-max-algorithm to reflect that different parties have opposing goals.

An exeption of this rule is if the opposing party is an ai-controlled entity (or entities) that follow strict rules without any freedom/randomness.

### Incomplete information

The solver relies on complete information. That means that any puzzles that work based on hiding some information from the player won't be applicable to the framework. Good examples for that would be most card games like Solitaire. If however you can provide all information to the solver, it can provide a valid solution. The problem just is that for regular Solitaire games you will never know the hidden cards without playing and when you started playing, you usually can't restart the same level. And each Solitaire game is different.