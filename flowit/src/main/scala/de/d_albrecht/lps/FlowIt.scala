/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps

/**
	Contains general type and function definitions important to the flowit-adaption,
	as well as the modeling of the game state type.
*/
object Flowit {
	/** a tuple type for two dimensional coordinates */
	type Pos = (Int, Int)
	/** the static portions of the level state */
	case class Fundament(size: Pos, colors: Map[Int, List[Pos]])
	/** the dynamic portions of the level state */
	case class State(cells: Map[Pos, Either[Int, String]])
	/** converts 2d-coordinates into their spreadsheet-like address */
	def stringify(pos: Pos): String = {
		('A'.toInt - 1 + pos._1).toChar.toString + pos._2
	}
	/** converts spreadsheet-like addresses into their 2d-coordinate format */
	def parsePos(s: String): Pos = {
		(s(0).toInt - 'A'.toInt + 1, s.tail.toInt)
	}
}