/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package flowit

import core._
import Flowit._

/** Represents all possible actions of Flowit and how they change a game state. */
object FlowitActions {
	/** contains the correct rotated spin actions */
	private val spinRepl: Map[Pos, String] = Map((0, -1) -> "e", (1, 0) -> "s", (-1, 0) -> "n", (0, 1) -> "w")
	/** contains the cartesian directions */
	private val dirs = spinRepl.toList.map(_._1)
	/** bombs a certain cell */
	def bomb(level: Fundament, state: State, actor: Pos): State = {
		val repl = Left(level.colors.toList.filter(_._2.contains(actor)).map(_._1).head)
		val pos = for {x <- -1 to 1; y <- -1 to 1 if state.cells.isDefinedAt((actor._1 + x, actor._2 + y))}
			yield (actor._1 + x, actor._2 + y)
		State(pos.foldLeft(state.cells){case (s, c) => s + (c -> repl)})
	}
	/** recursively floods the grid from one cell */
	@scala.annotation.tailrec
	private def floodRec(state: Map[Pos, Either[Int, String]], cells: List[Pos], search: Either[Int, String], replace: Either[Int, String]): Map[Pos, Either[Int, String]] = {
		cells match {
			case Nil => state
			case head :: tail => {
				if (!state.isDefinedAt(head) || state(head) != search) {
					floodRec(state, tail, search, replace)
				} else
					floodRec(state + (head -> replace), tail ++ dirs.map(dir => (head._1 + dir._1, head._2 + dir._2)), search, replace)
			}
		}
	}
	/** floods the grid from one cell */
	def flood(level: Fundament, state: State, actor: Pos): State = {
		val color = Left(level.colors.toList.filter(_._2.contains(actor)).map(_._1).head)
		val erase = !dirs.exists(dir => state.cells.isDefinedAt((actor._1 + dir._1, actor._2 + dir._2)) && state.cells((actor._1 + dir._1, actor._2 + dir._2)) == Left(0))
		State(floodRec(state.cells, dirs.map(dir => (actor._1 + dir._1, actor._2 + dir._2)), if (erase) color else Left(0), if (erase) Left(0) else color))
	}
	/** recursively floods a row of cells in some direction */
	@scala.annotation.tailrec
	private def lineRepl(state: Map[Pos, Either[Int, String]], cell: Pos, dir: Pos, search: Either[Int, String], replace: Either[Int, String]): Map[Pos, Either[Int, String]] = {
		if (!state.isDefinedAt(cell) || state(cell) != search) {
			state
		} else {
			lineRepl(state + (cell -> replace), (cell._1 + dir._1, cell._2 + dir._2), dir, search, replace)
		}
	}
	/** floods a row of cells in some direction */
	def line(level: Fundament, state: State, actor: Pos, dir: Pos): State = {
		val color = Left(level.colors.toList.filter(_._2.contains(actor)).map(_._1).head)
		// TODO: if the line action points to no cell at all, this will break
		val erase = state.cells((actor._1 + dir._1, actor._2 + dir._2)) == color
		State(lineRepl(state.cells, (actor._1 + dir._1, actor._2 + dir._2), dir, if (erase) color else Left(0), if (erase) Left(0) else color))
	}
	/** floods a row of cells in some direction and rotates the clickable cell */
	def spin(level: Fundament, state: State, actor: Pos, dir: Pos): State = {
		val intermediate = line(level, state, actor, dir)
		State(intermediate.cells + (actor -> Right(spinRepl(dir))))
	}
}
