/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package flowit

import core._
import Flowit._

/** Describes a level of Flowit as well as means to conveniently instantiate them. */
object FlowitLevel {
	/** takes a game level and adds explicit empty cells where they are not provided */
	def specifyEmptyCells(f: Fundament, cells: Map[Pos, Either[Int, String]]): State = {
		State(cells ++ f.colors.toList.flatMap(c => c._2).filterNot(c => cells.isDefinedAt(c)).map(c => c -> Left(0)).toMap)
	}
	/** instantiates a flowit level given the necessary information. empty cells don't need to be specified */
	def apply(fundament: Fundament, cells: Map[Pos, Either[Int, String]]): Level[Fundament, State] = Level(fundament, specifyEmptyCells(fundament, cells))
	/** represents the state serialization needed for the solver */
	val stateMap: (State) => String = (state) => state.cells.toList.sortBy(_._1).map(_._2 match{
		case Left(n) => n.toString
		case Right(a) => a
	}).mkString("")
}