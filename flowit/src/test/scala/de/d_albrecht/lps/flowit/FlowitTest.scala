/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package flowit

import core._
import Flowit._

object FlowitTest {
	def apply(level: Level[Fundament, State], exhaustive: Boolean = false): Boolean = {
		val game = new FlowitGame()
		val solver = new BfsSolver(game, level)
		solver.solve(exhaustive)
		val solutions = solver.getAllStates().filter(_ match {
			case RegularStep(_, _, `solved`, _) => true
			case DuplicateStep(_, RegularStep(_, _, `solved`, _)) => true
			case _ => false
		})
		//solutions.map(_.fullName()).foreach(println)
		solutions.size > 0
	}
	
	def fundament(lines: String*): Fundament = {
		val h = lines.size
		if (h == 0) {
			Fundament((0, 0), Map())
		} else {
			val w = lines.map(_.size).max(Ordering.Int)
			
			val cells = lines.zipWithIndex.flatMap{case (l, y) => l.toCharArray().toList.zipWithIndex.map{case (c, x) => (c, x + 1, y + 1)}}.filter(_._1.isDigit)
			val colors = cells.groupBy(_._1).map{case (color, cellList) => (color.toInt - '0'.toInt) -> cellList.map(c => (c._2, c._3)).toList.sorted}
			
			Fundament((w, h), colors)
		}
	}
	
	def state(lines: String*): State = {
		if (lines.size == 0) {
			State(Map())
		} else {
			val cells = lines.zipWithIndex.flatMap{case (l, y) => l.toCharArray().toList.zipWithIndex.map{case (c, x) => (c, x + 1, y + 1)}}.filter(c => c._1.isDigit || c._1.isLetter)
			val colors = cells.map(c => (c._2, c._3) -> (if (c._1.isDigit) Left(c._1.toInt - '0'.toInt) else Right(c._1.toString))).toMap
			
			State(colors)
		}
	}
	
	/** parses the state and fills empty cells with necessary information about empty cells */
	def state(fundament: Fundament, lines: String*): State = {
		val inter = state(lines: _*)
		FlowitLevel.specifyEmptyCells(fundament, inter.cells)
	}
}

class FlowitFixtureTest extends TestBase {
	"The FlowitTestFixture" should {
		"generate valid and intuitive Fundaments" in {
			val representation = List(
					"12345",
					"3.5.2",
					"51234",
					"2.4.1",
					"45123"
				)
			
			val expected = Fundament((5, 5), Map(1 -> List((1, 1), (2, 3), (3, 5), (5, 4)), 2 -> List((1, 4), (2, 1), (3, 3), (4, 5), (5, 2)), 3 -> List((1, 2), (3, 1), (4, 3), (5, 5)), 4 -> List((1, 5), (3, 4), (4, 1), (5, 3)), 5 -> List((1, 3), (2, 5), (3, 2), (5, 1))))
			
			FlowitTest.fundament(representation: _*) should be (expected)
		}
		
		"generate valid and intuitive States" in {
			val representation = List(
					"FSs4F",
					"3.5.W",
					"e1B3w",
					"E.4.1",
					"F5nNF"
				)
			
			val expected = State(Map((1, 1) -> Right("F"), (1, 2) -> Left(3), (1, 3) -> Right("e"), (1, 4) -> Right("E"), (1, 5) -> Right("F"), (2, 1) -> Right("S"), (2, 3) -> Left(1), (2, 5) -> Left(5), (3, 1) -> Right("s"), (3, 2) -> Left(5), (3, 3) -> Right("B"), (3, 4) -> Left(4), (3, 5) -> Right("n"), (4, 1) -> Left(4), (4, 3) -> Left(3), (4, 5) -> Right("N"), (5, 1) -> Right("F"), (5, 2) -> Right("W"), (5, 3) -> Right("w"), (5, 4) -> Left(1), (5, 5) -> Right("F")))
			
			FlowitTest.state(representation: _*) should be (expected)
		}
		
		"generate valid and intuitive States with not colored cells" in {
			val fundament = FlowitTest.fundament(
					"111",
					"1.1",
					"111",
				)
			val representation = List(
					"E.S",
					"...",
					"N.W"
				)
			
			val expected = State(Map((1, 1) -> Right("E"), (1, 2) -> Left(0), (1, 3) -> Right("N"), (2, 1) -> Left(0), (2, 3) -> Left(0), (3, 1) -> Right("S"), (3, 2) -> Left(0), (3, 3) -> Right("W")))
			
			FlowitTest.state(fundament, representation: _*) should be (expected)
		}
	}
}