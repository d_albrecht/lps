/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package flowit

import core._

class FlowitGameTest extends TestBase {
	"The Flowit game" should {
		"be able to create string representations for states" in {
			val uut = new FlowitGame()
			
			val state = FlowitTest.state(
					"135",
					"WE."
				)
			
			uut.stateMap(state) should be ("1W3E5")
		}
		
		"list no possible actions for no clickable cells" in {
			val uut = new FlowitGame()
			
			// the fundament isn't relevent for the listPossibleActions-method
			val fundament = FlowitTest.fundament()
			val state = FlowitTest.state(
					"135",
					"20."
				)
			
			uut.listPossibleActions(Level(fundament, state), state) should be (Nil)
		}
		
		"list all possible actions given available clickable cells" in {
			val uut = new FlowitGame()
			
			// the fundament isn't relevent for the listPossibleActions-method
			val fundament = FlowitTest.fundament()
			val state = FlowitTest.state(
					"135",
					"WE."
				)
			
			uut.listPossibleActions(Level(fundament, state), state) should be (List("A2", "B2"))
		}
		
		"bomb cells given matching action" in {
			val uut = new FlowitGame()
			
			val fundament = FlowitTest.fundament(
					"442",
					"24.",
					".4."
				)
			val state = FlowitTest.state(
					"13w",
					"BF.",
					".5."
				)
			val expected = (active, FlowitTest.state(
					"22w",
					"22.",
					".2."
				))
			
			uut.executeAction(Level(fundament, state), state, "A2") should be (expected)
		}
		
		"flood cells given matching action" in {
			val uut = new FlowitGame()
			
			val fundament = FlowitTest.fundament(
					"442",
					"244",
					"..4"
				)
			val state = FlowitTest.state(
					"000",
					"F42",
					"..0"
				)
			val expected = (active, FlowitTest.state(
					"222",
					"F42",
					"..0"
				))
			
			uut.executeAction(Level(fundament, state), state, "A2") should be (expected)
		}
		
		"fill rows given matching action" in {
			val uut = new FlowitGame()
			
			val fundament = FlowitTest.fundament(
					"442",
					"244",
					"..4"
				)
			val state = FlowitTest.state(
					"040",
					"e02",
					"..0"
				)
			val expected = (active, FlowitTest.state(
					"040",
					"s22",
					"..0"
				))
			
			uut.executeAction(Level(fundament, state), state, "A2") should be (expected)
		}
		
		"reverse flood cells given matching action" in {
			val uut = new FlowitGame()
			
			val fundament = FlowitTest.fundament(
					"442",
					"244",
					"..4"
				)
			val state = FlowitTest.state(
					"222",
					"F44",
					"..2"
				)
			val expected = (active, FlowitTest.state(
					"000",
					"F44",
					"..2"
				))
			
			uut.executeAction(Level(fundament, state), state, "A2") should be (expected)
		}
	}
}