/**
    Logic Puzzle Solver
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.lps
package flowit

import core._

//temporarily disabled because these levels need too much memory in a bfs search
@org.scalatest.Ignore
class FlowitHard extends TestBase {
	"From the hard levels of Flowit the solver" should {
		"be able to solve level 01" in {
			val fundament = FlowitTest.fundament(
					"11.11",
					"22111",
					"22222",
					"11111",
					"12233",
					"12.23"
				)
			val state = FlowitTest.state(fundament,
					"S..FB",
					".B..W",
					"....W",
					"....W",
					"..F..",
					".N.NF"
				)

			FlowitTest(Level(fundament, state)) should be (true)
		}
	}
}