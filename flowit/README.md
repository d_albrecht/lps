Logic Puzzle Solver for Flowit
==============================

This project contains the application of the solver framework to the mobile game [Flowit](https://github.com/Flowit-Game/Flowit).

I'm not associated with the game or its developers in any way.

## Flowit's objective

To quickly outline the objective of Flowit - in case you don't know the game and don't want to check the game out right now - is to color cells of a cartesian grid in the correct color. Each cell will define one target color our of five colors, some cells might be already colored either in their target color or a different color. To solve the level, some cells are clickable and will effect the grid in different ways. Some clickable cells will flood the surrounding cells in their color. Some will color a row (or column) of cells. Some will color a row and then turn by 90°. All of these types will color as much as they can until there are no more valid cells to color. A valid cell is always an un-colored cell, cells that are already colored will not change with these three types. If a clickable cell can't (or thinks it can't) color surrounding cells, it will instead remove the color of surrounding cells according to the same rules. It will only remove its own color from surrounding cells, but leave cells alone that are either not colored or colored in different colors. Then there is a fourth type of clickable cells, the bomb type. This type will put its color on the nine cells surrounding the bomb (including itself) and remove any clickable cells (including itself). If a cell loses its clickable type, it is turned into a regular cell with the color of the former clickable type being its target color. The grid can contain holes.

## Adapting the Flowit game to the solver Framework

As described by the framework description you have to model and implement several aspects of the actual puzzle game to be able to solve a level. In this segment I will describe the process and decisions as detailed as possible.

### Game state

For Flowit the game state at first seem pretty easily classifiable into static parts and dynamic parts. But that distinction gets obsolete as soon as you get to the bomb cells. Because they can remove other clickable cells as well and can only be activated once. Because of that the static part is rather sparse with the dynamic part containing a lot of information. For levels without bombs (and another type of rotating clickable cells) all clickable cells could be modeled as part of the static portion, but to make the model simpler, clickable cells will always be modeled as dynamic.

#### Fundament

The static level state part in Flowit contains the maximum dimensions of the level (although level maps can contain "holes"), as well as a list of colors with a list of cells each, describing which color these cells should have in order to complete the level. The colors of clickable cells will also be included in this list, because this entry describes which color the clickable cell uses to execute its filling and in case the clickable cell gets removed (by a bomb) the cell itself keeps it desired coloring. A color is just represented by a number.

#### Variable state

The dynamic state portion contains a list of all cells and their current state. This state can either describe the cell as containing one of the possible clickable cells, or as being colored with some color code. Empty and uncolored cells will be represented by the color code `0` (that should not be used as a regular color code).

### State serialization

A state gets serialized by converting every cell into one character, either a letter for a clickable cell, or a number for any colored (or uncolored) cell in the natural order of the cell addresses.

### Actions

Possible actions in each state is any clickable cell represented by its spreadsheet-like address (A1, B5, ...). Whether clicking a clickable cell would actually change the state or not will not be considered. On the one hand because that would require additional logic, on the other hand, because for the original puzzle game clicking cells that don't change anything still count as legal moves.

### Performing an action

Performing an action is simply defined by figuring out what type of clickable cell is at this positon, coloring cells accordingly, and possibly changing the clickable cell type itself.

## Test cases

I've included some (mostly random) selection of game levels as test cases.

## Limitations

Flowit levels can have up to five colors and around 48 cells (I don't know whether there is a upper limit, but 8x6 levels are definitely possible). But 5^48 is way to high to actually solve the game exhaustively. Based on this realization and the fact that some levels are just not solvable on reasonable powerful machines, I started to develop a solver that will utilize a heuristic to coordinate which states to further analyze first. See the discussion in the framework folder, but the gist of it is, that each puzzle game (or the developer adapting this game to the framework, to be precise) has to come up with this heuristic itself (the framework isn't capable of assigning any fitness value to a state). And for many games coming up with a useful heuristic can easily be more complex than manually solving every game level.